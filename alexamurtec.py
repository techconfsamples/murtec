"""
This is the logic for sample MURTEC Alexa skill
"""

from __future__ import print_function

import boto3
import datetime as dt
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr
from random import randint


# --------------- Helpers that build all of the responses ----------------------

def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "MURTEC - " + title,
            'content': "MURTEC - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }


# --------------- Functions that control the skill's behavior ------------------

def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """

    session_attributes = {}
    card_title = "Welcome"
    speech_output = "Welcome to the MURTEC Conference Alexa Skill. " \
                    " I can tell you about MURTEC and provide details on the speakers and their sessions. " \
                    " You can start by saying, tell me about MURTEC or tell me about a speaker."
    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = " You can say something like, when is MURTEC or tell me about Kesha Williams. "
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def handle_session_end_request():
    card_title = "Session Ended"
    speech_output = "Thank you for the MURTEC Conference Alexa Skill. " \
                    "We hope to see you at MURTEC again next year. "
    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))


def get_aboutmurtec_intent():
    session_attributes = {}
    reprompt_text = None
    should_end_session = True
    card_title = "MURTEC"
    speech_output = "At MURTEC, We’ll get elbows-deep in what’s possible - both today and in the future - as technology infiltrates every aspect of the restaurant industry. Learn how to power up today’s innovative solutions and prepare for what’s possible tomorrow."

    # Setting reprompt_text to None signifies that we do not want to reprompt
    # the user. If the user does not respond or says something that is not
    # understood, the session will end.
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def get_whenmurtec_intent():
    session_attributes = {}
    reprompt_text = None
    should_end_session = True
    card_title = "MURTEC"
    speech_output = "MURTEC is March 11 - 13th, 2019 at the Paris Hotel, Las Vegas."

    # Setting reprompt_text to None signifies that we do not want to reprompt
    # the user. If the user does not respond or says something that is not
    # understood, the session will end.
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def get_speaker_intent(intent):
    print(intent)
    session_attributes = {}
    reprompt_text = None
    should_end_session = True
    card_title = "MURTEC"

    speakerdict = {
        "Kesha": "Kesha Williams is a software engineering manager with Chick-fil-A. Here topic is Innovate & Iterate: How to Deploy Emerging Tech Rapidly with Real Results",
        "Dorothy": "Dorothy Creamer, editor of Hospitality Technology has been with HT for five years, starting as the publication’s managing editor. She's speaking at multiple sessions.",
        "Abby": "Abigail Lorden is a publishing executive with deep domain expertise in technology and business trends. She's speaking at several sessions.",
        "Donna": "Donna Cobb is the Executive Director, Strategic Partnerships and Influencer Marketing at Comcast. She's speaking about Leveraging Tech to Power Guest Loyalty & Experience "
    }

    random_num = randint(0, len(speakerdict) - 1)
    print("random_num  " + str(random_num))

    # Check to see if a value is present for speaker
    if 'value' in intent['slots']['speaker']:
        speaker_name = intent['slots']['speaker']['value']
        speech_output = speakerdict[speaker_name]
    else:
        speech_output = speakerdict["Kesha"]
        # Loop thru and return the one determined by random number
        for index, (key, value) in enumerate(speakerdict.items()):
            if index == random_num:
                speech_output = value

    # Setting reprompt_text to None signifies that we do not want to reprompt
    # the user. If the user does not respond or says something that is not
    # understood, the session will end.
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


# --------------- Events ------------------

def on_session_started(session_started_request, session):
    """ Called when the session starts """

    print("on_session_started requestId=" + session_started_request['requestId']
          + ", sessionId=" + session['sessionId'])


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they
    want
    """

    print("on_launch requestId=" + launch_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # Dispatch to your skill's launch
    return get_welcome_response()


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']

    # Dispatch to your skill's intent handlers
    if intent_name == "AboutMurtecIntent":
        return get_aboutmurtec_intent()
    elif intent_name == "WhenMurtecIntent":
        return get_whenmurtec_intent()
    elif intent_name == "GetSpeakerIntent":
        return get_speaker_intent(intent)
    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        raise ValueError("Invalid intent")


def on_session_ended(session_ended_request, session):
    """ Called when the user ends the session.

    Is not called when the skill returns should_end_session=true
    """
    print("on_session_ended requestId=" + session_ended_request['requestId'] +
          ", sessionId=" + session['sessionId'])


# --------------- Main handler ------------------

def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """
    print("event.session.application.applicationId=" +
          event['session']['application']['applicationId'])

    """
    prevent someone else from configuring a skill that sends requests to this
    function.
    """
    if (event['session']['application']['applicationId'] !=
            "amzn1.ask.skill.d3b1ef66-e01a-4a5e-9cb9-6adb00856a37"):
        raise ValueError("Invalid Application ID")

    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']},
                           event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])